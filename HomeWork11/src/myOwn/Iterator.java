package myOwn;

public interface Iterator {
    int next();
    boolean hasNext();
}
