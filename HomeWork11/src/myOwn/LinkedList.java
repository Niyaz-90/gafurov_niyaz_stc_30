package myOwn;

import java.util.NoSuchElementException;

public class LinkedList implements List {
    private Node first;
    private Node last;
    private int count;

    private class LinkedListIterator implements Iterator{
        LinkedListIterator linkedListIterator;
        private int currentIndex = 0;
        Node current;
        Node nextCurrent = first;

        @Override
        public int next() {
                while (hasNext()) {
                    current = nextCurrent;
                    nextCurrent = current.next;
                    this.currentIndex++;
                    return current.value;
                }
             throw new NoSuchElementException();

        }

        @Override
        public boolean hasNext() {
            return currentIndex < count;
        }
    }

    @Override
    public Iterator iterator() {
        return new LinkedListIterator();
    }

    public static class Node{
        int value;
        Node next;
        Node prev;

        public Node(int value) {
            this.value = value;
        }

        public Node(){
            this.next = next;
            this.prev = prev;
            this.value = value;
        }
    }

    @Override
    public int get(int index) {
        if(index >= 0 && index < count && first != null) {
            int i = 0;

            Node current = this.first;
            while (i < index) {
                current = current.next;
                i++;
            }
            return current.value;
        }
        System.err.println("Элемент не найден");
        return -1;
    }

    @Override
    public int indexOf(int element) {
        Node current = this.first;
        int i = 0;

        while(current != null && current.value != element){
            current = current.next;
            i++;
        }

        if (current == null) {
           // System.out.println("Элемент не найден");
            return -1;
        } else {
            return i;
        }
    }

    @Override
    public void add(int element) {

        Node newNode = new Node(element);
        if (first == null){
            first = newNode;
            last = newNode;
        }else {
            last.next = newNode;
            last = newNode;
//            last.prev =
        }
        count++;
//        java.util.List
    }

    @Override
    public boolean contains(int element) {
        if(indexOf(element) != -1){
            return true;
        } else
        return false;
    }

    @Override
    public int size() {
        return count;
    }

    @Override
    public void remove(int index) {
        Node current = first;
        Node nextCurrent = first;
        if(get(index) != -1){
            while (get(index) != current.value){
                current = nextCurrent;
                nextCurrent = current.next;
            }
            int i = 0;
            while (i < count - 2) {
                current = current.next;
                i++;
            }
            count--;
        }

        else {
            System.err.println("Index not found");
        }



    }

    @Override
    public void insert(int element, int index) {

        Node current = first;
        Node nextCurrent = first;
        Node nextAfterNew;
        if(first != null){
            int i = 0;
            while (i != index + 1) {
                current = nextCurrent;
                nextCurrent = current.next;
                i++;
            }
            nextAfterNew = current.next;
//            Node prevCurrent = current.prev;
            Node newNode = new Node();
            current.next = newNode;


//            newNode = current.next;
            newNode.next = nextAfterNew;
            newNode.value = element;
//            newNode.prev = current.prev;

            this.count++;


        }


    }

    @Override
    public String toString() {
        StringBuilder stringBuilder = new StringBuilder();
        Iterator iterator = this.iterator();
        while (iterator.hasNext()){
            stringBuilder.append(iterator.next()).append(" ");
        }
        return "LinkedList{ " + stringBuilder.toString() + "}";
    }
}
