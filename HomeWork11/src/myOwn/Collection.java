package myOwn;

public interface Collection extends Iterable {
    void add(int element);
    boolean contains(int element);
    int size();
    void remove(int element);
}
