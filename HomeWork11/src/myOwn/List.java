package myOwn;

public interface List extends Collection{
    int get(int index);
    int indexOf(int element);
    void remove(int index);
    void insert(int element, int index);

}
