package myOwn;

public class ArrayList implements List{
    public static final int DEFAULT_SIZE = 10;
    public int[] data;
    private int count;

    public ArrayList() {
        this.data = new int[DEFAULT_SIZE];
    }

    @Override
    public Iterator iterator() {
        return new ArrayListIterator();
    }

    public class ArrayListIterator implements Iterator{
        private int current = 0;

        @Override
        public int next() {
            int value = data[current];
            current++;
            return value;
        }

        @Override
        public boolean hasNext() {
            return current < count;
        }
    }
    @Override
    public int get(int index) {
        if(index < count){
            return data[index];
        }
        return -1;
    }

    @Override
    public int indexOf(int element) {

            for (int i = 0; i < data.length; i++) {
                if (data[i] == element) {
                    return i;
                }
            }
//            else{
//            System.err.println("Элемент не найден");
            return -1;

    }

    @Override
    public void add(int element) {
       if (count == data.length - 1){
           resize();
       }
       data[count] = element;
       count++;

    }

    public void resize(){
        int oldLength = this.data.length;
        int newLength = oldLength + (oldLength >> 1);
        int[] newData = new int[newLength];
        System.arraycopy(this.data,0,newData, 0, oldLength);
        this.data = newData;
    }

    @Override
    public boolean contains(int element) {

        return indexOf(element) != -1;
    }

    @Override
    public int size() {
        return this.count;
    }

    @Override
    public void remove(int index) {
        for(int i = index; i < count - 1; i++){
            this.data[i] = this.data[i + 1];
        }
        this.count--;

    }

    @Override
    public void insert(int element, int index) {
        if (count + 1 >= data.length){
            resize();
        }



            for (int i = count + 1; i >= index; i--) {
                data[i] = data[i - 1];
            }

            data[index] = element;
            count++;


    }


    @Override
    public String toString() {
        StringBuilder stringBuilder = new StringBuilder();
        Iterator iterator = this.iterator();
        while (iterator.hasNext()){
            stringBuilder.append(iterator.next()).append(" ");
        }
        return "ArrayList{ " + stringBuilder.toString() + "}";
    }
}
