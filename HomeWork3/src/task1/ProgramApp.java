package task1;

import java.util.Arrays;
import java.util.Scanner;

public class ProgramApp {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int sum = 0;
        int[] arrayNumber = null;
        int intNumber = 0;
        int i;
        int[] turnedArray = null;
        int[] array = null;
        int[] arrayBubble = null;

        System.out.println("Change function:");
        System.out.println("1. Sum of array's elements.");
        System.out.println("2. Turn array.");
        System.out.println("3. Calculate the average of array's elements.");
        System.out.println("4. Moving Max and Min elements of array.");
        System.out.println("5. Sorting array's element from Min to Max with 'BUBBLE' method.");
        System.out.println("6. Transformation array to number.");
        int function = scanner.nextInt();
        int averageVariable = 0;


        if (function == 1) {
            System.out.println("Write size of array");

            System.out.println("the sum of array is: " + sumArray(sum));
        }


        else if(function == 2) {
            System.out.println("Write array");

            System.out.println(Arrays.toString(turnArray(turnedArray)));
        }


        else if (function == 3) {
            System.out.println("write array's size");

            System.out.println(averageArray(averageVariable));
        }


        else if (function == 4) {
            System.out.println("Write array's size");

            System.out.println(Arrays.toString(maxMinArray(array)));
        }


        else if (function == 5) {
            System.out.println("Write array's size: ");


            System.out.println(Arrays.toString(bubbleSort(arrayBubble)));
        }


        else if (function == 6) {
            System.out.println("Write array's size: ");

            //System.out.println(Arrays.toString(arrayNumber));
            System.out.println("Transformed number:" + arrayToNumber(intNumber));
        }


        else {
            System.out.println("Don't understand, repeat your command");
        }
    }



    public static int sumArray(int a){

        Scanner scanner = new Scanner(System.in);
        int sum = 0;
        int i;
        int size = scanner.nextInt();
        int[] arraySum = new int[size];


        for (i = 0; i < arraySum.length; i++) {
            System.out.println("Write array elements");
            arraySum[i] = scanner.nextInt();
            sum = sum + arraySum[i];
        }

        return sum;
    }



    public static int[] turnArray(int[] t) {

        Scanner scanner = new Scanner(System.in);
        int i;
        int size = scanner.nextInt();
        int[] turnedArray = new int[size];
        System.out.println("Write array elements");

        for (i = turnedArray.length - 1; i >= 0; i--) {
            turnedArray[i] = scanner.nextInt();
        }

        return turnedArray;
    }





    public static double averageArray(double a) {

        Scanner scanner = new Scanner(System.in);
        int i;
        int sum = 0;
        double averageVariable = 0;
        int size = scanner.nextInt();
        System.out.println("Write array's elements: ");
        int[] array = new int[size];

        for(i = 0; i < array.length; i++){
            array[i] = scanner.nextInt();
            sum = sum + array[i];
        }
        averageVariable = (double)sum / size;
        System.out.print("The average of array's elements is: ");


        return averageVariable;
    }




    public static int[] maxMinArray(int[] a) {

        Scanner scanner = new Scanner(System.in);
        int i;
        int temp;
        int tempMax = 0;
        int tempMin = 0;
        int size = scanner.nextInt();
        System.out.println("write array's elements");
        int[] array = new int[size];

        for(i = 0; i < array.length; i++){
            array[i] = scanner.nextInt();
        }
        System.out.println(Arrays.toString(array));

        for (i = 1; i < array.length; i++){
            if(array[i] > array[tempMax]) {
                tempMax = i;
            }
            else if (array[i] < array[tempMin]) {
                tempMin = i;
            }
        }

        temp = array[tempMax];
        array[tempMax] = array[tempMin];
        array[tempMin] = temp;

        System.out.print("The result array: ");

        return array;
    }



    public static int[] bubbleSort(int[] b) {

        Scanner scanner = new Scanner(System.in);
        int i;
        int j;
        int temp;
        int size = scanner.nextInt();
        System.out.println("write array's elements");
        int[] arrayBubble = new int[size];


        for(i = 0; i < arrayBubble.length; i++){
            arrayBubble[i] = scanner.nextInt();
        }


        for(i = arrayBubble.length; i >= 0; i--){
            for(j = 1; j < arrayBubble.length; j++){
                if(arrayBubble[j] < arrayBubble[j - 1]) {
                    temp = arrayBubble[j - 1];
                    arrayBubble[j - 1] = arrayBubble[j];
                    arrayBubble[j] = temp;
                }
            }
        }


        System.out.print("Sorted array:");

        return arrayBubble;
    }




    public static int arrayToNumber(int a){

        Scanner scanner = new Scanner(System.in);
        int i;
        String strNumber = "";
        int intNumber = 0;
        int intNumber1 = 0;
        int size = scanner.nextInt();
        System.out.println("Enter array's elements: ");
        int[] arrayNumber = new int[size];

        for (i = 0; i < arrayNumber.length; i++) {
            arrayNumber[i] = scanner.nextInt();
            strNumber = strNumber + arrayNumber[i];
            intNumber = Integer.parseInt(strNumber);
        }


        System.out.print("Entered array: ");
        System.out.println(Arrays.toString(arrayNumber));

        return intNumber;
    }
}
