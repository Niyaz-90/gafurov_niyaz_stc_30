package graphs;

public class EdgeAndTwoVerteciesImpl implements Edge {
    private Vertex firstVertex;
    private Vertex secondVertex;
    private int weight;

    public EdgeAndTwoVerteciesImpl(Vertex firstVertex, Vertex secondVertex, int weigth) {
        this.firstVertex = firstVertex;
        this.secondVertex = secondVertex;
        this.weight = weigth;
    }

    @Override
    public Vertex getFirst() {
        return firstVertex;
    }

    @Override
    public Vertex getSecond() {
        return secondVertex;
    }

    @Override
    public int getWeigth() {
        return weight;
    }
}
