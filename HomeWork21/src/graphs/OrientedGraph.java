package graphs;

import java.util.ArrayList;
import java.util.List;

public interface OrientedGraph {
//    void addVertex(List<ArrayList<Integer>> list, Vertex vertex1, Vertex vertex2);
    void addEdge(Edge edge );

    void bfs(int vertex);
    void dfs(int vertex);

}
