package graphs;

import java.util.*;

public class OrientedGraphAdjacencyImpl implements OrientedGraph {
    private LinkedList<Integer> adjacencylist[];
    private int adjacencyListsize;

    public OrientedGraphAdjacencyImpl(int adjacencyListsize) {
        this.adjacencyListsize = adjacencyListsize;
        adjacencylist = new LinkedList[adjacencyListsize];
        for (int i = 0; i < adjacencyListsize; i++) {
           adjacencylist[i] = new LinkedList<>();
        }
    }



//    @Override
//    public void addVertex(List<ArrayList<Integer>> list, Vertex vertex1, Vertex vertex2) {
//        adjacencylist[vertex1.getNumber()].add(vertex2.getNumber()) ;
//    }



    @Override
    public void addEdge( Edge edge) {
        adjacencylist[edge.getFirst().getNumber()].add(edge.getSecond().getNumber()) ;
    }




    @Override
    public void bfs(int vertex) {

        boolean[] visited = new boolean[adjacencyListsize];
        LinkedList<Integer> queue = new LinkedList<Integer>();
        visited[vertex] = true;
        queue.add(vertex);
        while (queue.size() != 0){
            vertex = queue.poll();
            System.out.print(vertex + " ");
            Iterator<Integer> i = adjacencylist[vertex].listIterator();
            while (i.hasNext()){
                int n = i.next();
                if (!visited[n]){
                    visited[n] = true;
                    queue.add(n);
                }
            }
        }

    }

    @Override
    public void dfs(int vertex) {
        Stack<Integer> stack = new Stack<Integer>();
        boolean[] isVisited = new boolean[adjacencylist.length];
        stack.push(vertex);
        while (!stack.isEmpty()){
            int current = stack.pop();
            if(!isVisited[current]){
                isVisited[current] = true;
                System.out.println(current + " ");
                Stack<Integer> aux = new Stack<Integer>();
                for (int neighbour : adjacencylist[current]){
                    if (!isVisited[neighbour]){
                        aux.push(neighbour);
                    }
                }
                while (!aux.isEmpty()){
                    stack.push(aux.pop());
                }
            }
        }
        System.out.println();


    }
}
