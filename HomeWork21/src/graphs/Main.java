package graphs;

import java.util.ArrayList;

public class Main {
    public static void main(String[] args) {

        ArrayList<ArrayList<Integer>> adjacencyList = new ArrayList<ArrayList<Integer>>();
        Vertex vertex0 = new IntegerVertexImpl(0);
        Vertex vertex1 = new IntegerVertexImpl(1);
        Vertex vertex2 = new IntegerVertexImpl(2);
        Vertex vertex3 = new IntegerVertexImpl(3);
        Vertex vertex4 = new IntegerVertexImpl(4);
        Vertex vertex5 = new IntegerVertexImpl(5);
        Vertex vertex6 = new IntegerVertexImpl(6);
        Vertex vertex7 = new IntegerVertexImpl(7);
        Vertex vertex8 = new IntegerVertexImpl(8);
        Vertex vertex9 = new IntegerVertexImpl(9);
        Vertex vertex10 = new IntegerVertexImpl(10);

        Edge edge0 = new EdgeAndTwoVerteciesImpl(vertex0, vertex2, 1);
        Edge edge1 = new EdgeAndTwoVerteciesImpl(vertex2, vertex6, 1);
        Edge edge2 = new EdgeAndTwoVerteciesImpl(vertex6, vertex8, 1);
        Edge edge3 = new EdgeAndTwoVerteciesImpl(vertex6, vertex1, 1);
        Edge edge4 = new EdgeAndTwoVerteciesImpl(vertex1, vertex3, 1);
        Edge edge5 = new EdgeAndTwoVerteciesImpl(vertex5, vertex6, 1);
        Edge edge6 = new EdgeAndTwoVerteciesImpl(vertex2, vertex0, 1);
        Edge edge7 = new EdgeAndTwoVerteciesImpl(vertex1, vertex9, 1);

        OrientedGraph orientedGraph = new OrientedGraphAdjacencyImpl(11);
        orientedGraph.addEdge(edge0);
        orientedGraph.addEdge(edge1);
        orientedGraph.addEdge(edge2);
        orientedGraph.addEdge(edge3);
        orientedGraph.addEdge(edge4);
        orientedGraph.addEdge(edge5);
        orientedGraph.addEdge(edge6);
        orientedGraph.addEdge(edge7);

        orientedGraph.bfs(vertex2.getNumber());
        System.out.println();
        System.out.println("___________");
        orientedGraph.dfs(vertex2.getNumber());
    }
}
