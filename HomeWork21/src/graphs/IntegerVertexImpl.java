package graphs;

public class IntegerVertexImpl implements Vertex {
    int number;
    private boolean visitStatus = false;

    public IntegerVertexImpl(int number) {
        this.number = number;
    }

    @Override
    public int getNumber() {
        return number;
    }

    @Override
    public void isVisited() {
        this.visitStatus = true;
    }
    public void refreshVertexStatus(){
        this.visitStatus = false;
    }
}
