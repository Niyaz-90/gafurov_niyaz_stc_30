package graphs;

public interface Vertex {
    int getNumber();
    void isVisited();
}
