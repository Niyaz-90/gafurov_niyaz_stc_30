package tree;

import java.util.Deque;
import java.util.LinkedList;
import java.util.Objects;

public class TreeImpl<T extends Comparable<T>> implements Tree<T> {
    private Node<T> root;

    static class Node<E> {
        Node<E> left;
        Node<E> right;
        Node<E> parent;
        E value;

        @Override
        public String toString() {
            return value.toString();
        }

        public Node(E value) {
            this.value = value;
        }
    }


    @Override
    public void insert(T value) {
        this.root = insert(root, value);

    }

    private Node<T> insert(Node<T> root, T value) {


        if (root == null) {
            root = new Node<>(value);
//            return root;
        }


        else if (value.compareTo(root.value) < 0) {
            root.left = insert(root.left, value);

            root.left.parent = root;
        } else  {
            root.right = insert(root.right, value);

            root.right.parent = root;
        }
        return root;
    }

    @Override
    public void bfs() {
        bfs(root);

    }

    private void bfs(Node<T> root) {
        Deque<Node<T>> currentLevelQueue = new LinkedList<>();
        Deque<Node<T>> nextLevelQueue = new LinkedList<>();
        currentLevelQueue.add(root);

        Node<T> current;

        while (!currentLevelQueue.isEmpty()) {

            for (Node<T> nextCurrent : currentLevelQueue){
                current = nextCurrent;
                if (current.left != null){
                    nextLevelQueue.add(current.left);
                }

                if (current.right != null){
                    nextLevelQueue.add(current.right);
                }
                System.out.print(current.value + " ");
            }
            System.out.println();
            currentLevelQueue = nextLevelQueue;
            nextLevelQueue = new LinkedList<>();

        }
    }

    @Override
    public Node<T> dfs(T value) {
        dfs(root, value);
        return root;

    }

    private Node<T> dfs(Node<T> root, T value) {
        if (root != null | root.value != value) {
            dfs(root.left, value);
            System.out.println(root);
            dfs(root.right, value);
        }
        return root;
    }

    @Override
    public void remove(T value) {
        this.root = remove(this.root, value);
    }

    //    public void remove(T value) {
//
//        Node<T> parentOfCurrent;
//
//        Node<T> current = root ;
//
//        if (!contains(value)) {
//            System.out.println("Число "  + value + " отсутствует");
//
//            return;
//        }
//        else {
//            while (current.value != value) {
//                if (value.compareTo(current.value) < 0) {
//                    current = current.left;
//                } else if (value.compareTo(current.value) >= 0) {
//                    current = current.right;
//                }
//            }
//
//            if (current.left == null & current.right == null) {
//                parentOfCurrent = current.parent;
//                if (current.value.compareTo(parentOfCurrent.value) >= 0) {
//                    parentOfCurrent.right = null;
//                } else  {
//                    parentOfCurrent.left = null;
//                }
//            } else  {
//                parentOfCurrent = current.parent;
//                if (parentOfCurrent.value.compareTo(current.value) < 0) {
//                    if (current.left != null) {
//                        parentOfCurrent.left = current.right;
//                    } else {
//                        parentOfCurrent.left = current.left;
//                    }
//                } else {
//                    if (current.right != null) {
//                        parentOfCurrent.right = current.right;
//                    } else {
//                        parentOfCurrent.right = current.left;
//                    }
//                }
//
//            }
//
//        }
//    }
    public Node<T> remove(Node<T> root, T value) {
        if (root == null) {
            return root;
        } else if (value.compareTo(root.value) < 0) {
            root.left = remove(root.left, value);
        } else if (value.compareTo(root.value) > 0) {
            root.right = remove(root.right, value);
        } else if (root.left != null && root.right != null){


            root.value = findMin(root.right).value;
            root.right = remove(root.right, root.value);
        } else if (root.left != null) {
            root = root.left;
        } else if (root.right != null) {
            root = root.right;
        } else {
            root = null;
        }
        return root;


    }

    protected Node<T> findMin( Node<T> root) {

        if( root != null ) {

            while (root.left != null) {

                root = root.left;
            }
        }


        return root;

    }




    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        TreeImpl<?> tree = (TreeImpl<?>) o;
        return Objects.equals(root, tree.root);
    }

    @Override
    public int hashCode() {
        return Objects.hash(root);
    }

    @Override
    public boolean contains(T value) {

        return isContains(root, value) != null;

    }

    private Node<T> isContains(Node<T> root, T value) {

        while (root.value != value) {
            if (value.compareTo(root.value) < 0) {
                root = root.left;
            }
            if (root == null) {
                return null;
            }
            if (value.compareTo(root.value) >= 0) {
                root = root.right;
            }
            if (root == null) {
                return null;
            }



        }

        return root;
    }
}
