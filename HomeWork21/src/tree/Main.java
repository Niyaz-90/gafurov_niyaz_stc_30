package tree;

public class Main {

    public static void main(String[] args) {
        TreeImpl<Integer> tree = new TreeImpl<>();

//		tree.insert(8);
//		tree.insert(6);
//		tree.insert(9);
//		tree.insert(5);
//		tree.insert(7);
//		tree.insert(8);
//		tree.insert(10);

        tree.insert(3);
        tree.insert(2);
        tree.insert(5);
        tree.insert(1);
        tree.insert(3);
//        tree.insert(4);
        tree.insert(8);
        tree.insert(9);
        tree.insert(3);
        tree.insert(4);
        tree.insert(6);
        tree.insert(0);
        tree.insert(10);
        tree.insert(11);
        tree.insert(2);
        tree.insert(8);
        tree.insert(3);
        tree.bfs();
        System.out.println(tree.contains(5));
        System.out.println(tree.contains(10));
        System.out.println(tree.contains(2));
        System.out.println(tree.contains(4));
        System.out.println(tree.contains(18));

        System.out.println(tree.contains(11));

        tree.remove(4);
		System.out.println("4");
        tree.remove(5);
		System.out.println("5");
        tree.remove(11);
        tree.remove(99);
        tree.remove(8);
        tree.remove(9);
		System.out.println("10");

		System.out.println("___________");
		System.out.println(tree.contains(4));
		System.out.println(tree.contains(5));
		System.out.println(tree.contains(10));
		System.out.println(tree.contains(11));
		System.out.println(tree.contains(2));
//	tree.insert(3);


    }
}
