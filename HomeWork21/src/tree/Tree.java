package tree;

public interface Tree<T> {
    void insert(T value);
    void bfs();
    TreeImpl.Node<T> dfs(T value);
    void remove(T value);
    boolean contains(T value);
}
