package interfaces;

public class Rectangle extends GeometricalFigures implements Relocatable, Scalable{

    private double square;
    private double perimeter;
    protected double width;
    protected double heigth;



    public Rectangle(int x, int y, double width, double heigth){
        super(x, y);
        this.width = width;
        this.heigth = heigth;
    }


    public void squareCalculate(){


        square = width * heigth;

        System.out.println("Square of Rectangle is: " + square);

    }


    public void perimeterCalculate(){

        perimeter = (width + heigth) / 2;


        System.out.println("Perimeter of Circle is: " + perimeter);


    }

    @Override
    public void moveFigure() {
        this.x += 1;
        this.y += 1;
    }

    @Override
    public void scaleFigure(int scale) {

        this.width *= scale;
        this.heigth *= scale;
    }


}
