package interfaces;



public class Main {

    public static void main(String[] args) {

        Ellipse ellipse1 = new Ellipse(0,0, 4, 3);
        CircleImpl circle = new CircleImpl(0,0,5);
        Rectangle rectangle = new Rectangle(0,0,4,7);
        Square square = new Square(0,0, 5);

        System.out.println("Ellipse functions result");
        //Ellips
        ellipse1.perimeterCalculate();
        ellipse1.squareCalculate();
        ellipse1.scaleFigure(2);
        ellipse1.moveFigure();
        ellipse1.perimeterCalculate();
        ellipse1.squareCalculate();

        System.out.println();
        System.out.println("------------");


        //Circle
        System.out.println("Circle functions result");
        circle.perimeterCalculate();
        circle.squareCalculate();
        circle.scaleFigure(3);
        circle.moveFigure();

        System.out.println();
        System.out.println("------------");

        //Rectangle
        System.out.println("Rectangle functions result");
        rectangle.scaleFigure(3);
        rectangle.perimeterCalculate();

        System.out.println();
        System.out.println("------------");

        //Square
        System.out.println("Square functions result");
        square.moveFigure();
        square.squareCalculate();










    }
}
