package interfaces;

public class CircleImpl extends GeometricalFigures implements Relocatable, Scalable  {

    protected double square;
    protected double perimeter;
    protected double radius;

    public CircleImpl(int x,int y, int radius){
        super(x, y);
        this.radius = radius;

    }



    public void squareCalculate(){


        square = Math.PI * radius * radius;

        System.out.println("Square Circle is: " + square);

    }


    public void perimeterCalculate(){


        perimeter = Math.PI * 2 * radius;


        System.out.println("Perimeter of Circle is: " + perimeter);


    }

    @Override
    public void moveFigure() {
        this.x += 1;
        this.y += 1;
    }

    @Override
    public void scaleFigure(int scale) {

        this.radius *= scale;

    }


}
