package interfaces;

public  class Ellipse extends GeometricalFigures implements Relocatable, Scalable  {
    protected double square;
    protected double perimeter;
    public double width;
    public double heigth;


    public double getWidth() {
        return this.width;
    }

    public double getHeigth() {
        return this.heigth;
    }


    public Ellipse(int x, int y, double width, double heigth) {
        super(x, y);
        this.width = width;
        this.heigth = heigth;

    }


    public void squareCalculate() {


        this.square = Math.PI * width * heigth;
        System.out.println("Square of Ellipse is: " + square);

    }


    public void perimeterCalculate() {

        this.perimeter = Math.PI * 2 * Math.sqrt((width * width + heigth * heigth) / 2);


        System.out.println("Perimeter of Ellipse is: " + perimeter);


    }

    @Override
    public void moveFigure() {
        this.x += 1;
        this.y += 1;
    }

    @Override
    public void scaleFigure(int scale) {

        this.width *= scale;
        this.heigth *= scale;
    }
}
