package task1;


import java.util.Arrays;
import java.util.Scanner;

public class ProgramRecursion {

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int[] array = { -2, 4, 6, 8, 10, 12,15 };
        System.out.println(Arrays.toString(array));
        System.out.println("Take Number for search: ");
        int element = scanner.nextInt();
        //boolean existTreker = false;
        int left = 0;
        int right = array.length - 1;
        System.out.print("The index of element: ");
        System.out.println(binaryRecursion(array, element, left, right));
        if (binaryRecursion(array, element, left, right ) > 0){
            System.out.println("is exsist");
        }
        else {
            System.out.println("not exsist");
        }
    }

    public static int binaryRecursion(int[] array, int element, int left, int right) {
        int mid = left + (right - left) / 2;


        if (left <= right) {

            if (array[mid] == element) {
                return mid;
            } else if (array[right] == element) {
                return right;
            } else if (array[left] == element) {
                return left;
            } else if (element < array[mid] & mid >= left) {
                right = mid;

                return binaryRecursion(array, element, left, mid - 1);
            } else if (element > array[mid] & mid <= right) {
                left = mid;
                return binaryRecursion(array, element, mid + 1, right);
            }
        }
        return -888;
    }
}
