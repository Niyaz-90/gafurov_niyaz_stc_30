package hashSet;

import java.util.List;

public interface Set<V> {
    void add(V element);
    boolean contains(V value);
    List<V> getKeysSet(List<V> mapKist);
}
