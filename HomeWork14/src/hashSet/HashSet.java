package hashSet;

import hashMap.HashMapIml;

import java.util.*;

public class HashSet<K> implements Set<K> {

    private static final int DEFAULT_SIZE = 16;
    private K[] hashSetArray = (K[]) new Object[DEFAULT_SIZE];
    private transient HashMapIml<K, Object> map;
    public transient ArrayList<K> keyArray;
    private K constant = null;

    public HashSet() {
        map = new HashMapIml<>();
        keyArray = new ArrayList<>();
    }


        @Override
        public void add (K element){
            map.put(element, constant);

        }

        @Override
        public boolean contains (K value){
            int index = value.hashCode() & (hashSetArray.length - 1);
            if (hashSetArray[index] == null){
                return false;
            } else return hashSetArray.equals(value);
        }

    @Override
    public List<K> getKeysSet(List<K> mapList) {
        List<K> keysArray = new ArrayList<>();
        keysArray.addAll(map.keySet());

        return keysArray;

    }


}

