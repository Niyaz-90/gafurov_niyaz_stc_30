package hashMap;


import hashSet.Set;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;

public class HashMapIml<K, V> implements Map<K, V> {

    private static final int DEFAULT_SIZE = 16;
    private MapEntry<K, V>[] entries = new MapEntry[DEFAULT_SIZE];
    private int count = 0;
    private int i = 0;
    public List<K> mapList = new ArrayList<>();

    @Override
    public String toString() {
        return "HashMapIml{" +
                "entries=" + Arrays.toString(entries) +
                '}';
    }

    private static class MapEntry<K, V> {
        K key;
        V value;
        MapEntry<K, V> next;

        public MapEntry(K key, V value) {
            this.key = key;
            this.value = value;
        }
    }


    @Override
    public boolean put(K key, V value) {

        MapEntry<K, V> newMapEntry = new MapEntry<>(key, value);
        int index = key.hashCode() & (entries.length - 1);
        if (entries[index] == null) {
            entries[index] = newMapEntry;
            count++;
        } else {
            MapEntry<K, V> current = entries[index];
            while (current != null) {
                if (current.key.equals(key)) {
                    current.value = value;
                    return true;
                }
                current = current.next;
            }

            current = newMapEntry;
        }
        return false;

    }

    @Override
    public V get(K key) {

        int index = key.hashCode() & (entries.length - 1);
        MapEntry<K, V> current = entries[index];
        while (current != null) {
            if (current.key.equals(key)) {
                return current.value;
            }
            current = current.next;
        }
        System.out.println("error key");
        return null;
    }


    private static class SetEntry<K> {
        K key;
        SetEntry<K> next;

        public SetEntry(K key) {
            this.key = key;
        }
    }


    public List<K> keySet() {
        MapEntry<K, V> current;
        for (MapEntry<K, V> entry : entries) {
            current = entry;
            if (current != null) {
                mapList.add(current.key);
                while (current.next != null) {
                    current = current.next;
                    mapList.add(current.key);
                }
            }
        }
        return mapList;

    }

}
