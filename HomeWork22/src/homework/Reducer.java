package homework;

public class Reducer extends Thread {
    ArrayManager arrayManager;

    public Reducer(ArrayManager arrayManager) {
        this.arrayManager = arrayManager;
    }

    @Override
    public void run() {
        synchronized (arrayManager) {
            arrayManager.reduce();
        }
    }

}
