package homework;

import javafx.scene.transform.Scale;

import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        int[] array = {0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14};
        ArrayManager arrayManager = new ArrayManager(array);
        Reducer reducer = new Reducer(arrayManager);
        Reducer reducer1 = new Reducer(arrayManager);
        Reducer reducer2 = new Reducer(arrayManager);


            reducer.start();
            reducer1.start();
            reducer2.start();
        try {
            reducer.join();
            reducer1.join();
            reducer2.join();
        } catch (InterruptedException e) {
            throw new IllegalStateException(e);
        }

        System.out.println(arrayManager.getSum());

    }
}
