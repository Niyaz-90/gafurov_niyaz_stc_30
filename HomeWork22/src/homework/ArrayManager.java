package homework;

public class ArrayManager {
    private int[] array;
    private int result1 = 0;
    private int result2 = 0;
    private int result3 = 0;
    private int sum;

    public int getSum() {
        return result1 + result2 + result3;
    }

    public ArrayManager(int[] array) {
        this.array = array;
    }

    public void reduce(){
        if(Thread.currentThread().getName().equals("Thread-0")){
            for (int i = 0; i < 5 ; i++) {
                this.result1 += array[i];

                System.out.println(Thread.currentThread().getName() + " element value: " + array[i]);

            }
        }

        if(Thread.currentThread().getName().equals("Thread-1")){
            for (int i = 5; i < 10 ; i++) {
                this.result2 += array[i];

                System.out.println(Thread.currentThread().getName() + " element value: " + array[i]);

            }
        }

        if(Thread.currentThread().getName().equals("Thread-2")){
            for (int i = 10; i < 15 ; i++) {
                this.result3 += array[i];

                System.out.println(Thread.currentThread().getName() + " element value: " + array[i]);

            }
        }
    }

    @Override
    public String toString() {
        return "ArrayManager{" +
                "sum = " + sum +
                '}';
    }
}
