package task1;

import java.util.Arrays;
import java.util.Scanner;

public class ProgramArray {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int size = scanner.nextInt();
        int[] array = new int [size];
        int arraySumma = 0;
        int i;
        for( i = 0;i < array.length; i++) {
            array[i] = scanner.nextInt();
            arraySumma = arraySumma + array[i];
        }

        System.out.println(Arrays.toString(array));
        System.out.println(arraySumma);

        }
}
