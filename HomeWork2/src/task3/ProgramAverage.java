package task3;

import java.util.Arrays;
import java.util.Scanner;

public class ProgramAverage {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int size = scanner.nextInt();
        int[] array = new int[size];
        float arrayAverage = 0;
        int i;
        for(i = 0; i < array.length; i++){
            array[i] = scanner.nextInt();
            arrayAverage = arrayAverage + array[i];
        }
        arrayAverage = arrayAverage / array.length;
        System.out.println(Arrays.toString(array));
        System.out.println("Среднее арифметическое чисел массива: " + arrayAverage);
    }
}
