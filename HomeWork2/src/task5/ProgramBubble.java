package task5;

import java.util.Arrays;
import java.util.Scanner;

public class ProgramBubble {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int size = scanner.nextInt();
        int[] array = new int[size];
        int i;
        int j;
        for(i = 0; i < array.length; i++){
            array[i] = scanner.nextInt();
        }
        for(i = size - 1; i >= 0; i--){
            for(j = 0; j < size - 1; j++){
                if(array[j] >= array[j + 1]){
                    int timely = array[j];
                    array[j] = array[j + 1];
                    array[j + 1] = timely;
                }

            }
        }
        System.out.println(Arrays.toString(array));
    }
}
