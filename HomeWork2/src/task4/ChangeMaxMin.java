package task4;

import java.util.Arrays;

public class ChangeMaxMin {
    public static void main(String[] args) {

        int[] a = {36, 9, 6, 6, 25};
        int i;
        int indexMax = 0;
        int indexMin = 0;
        System.out.println(Arrays.toString(a));
        System.out.println();

        for (i = 0; i < a.length; i++){
            if (a[i] < a[indexMin]){
                indexMin = i;
            }
            else if (a[i] > a[indexMax]) {
                indexMax = i;
            }
            else {
                i = i + 0;
            }
        }
        System.out.println(indexMax);

        System.out.println(indexMin);


        int timely = a[indexMax];
        a[indexMax] = a[indexMin];
        a[indexMin] = timely;


        System.out.println(Arrays.toString(a));
    }
}
