package task2;

import java.util.Arrays;
import java.util.Scanner;

public class ProgramTurnArray {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int size = scanner.nextInt();
        int[] a = new int[size];
        int i;
        for (i = a.length - 1; i >= 0; i--) {
            a[i] = scanner.nextInt();
        }
        System.out.println(Arrays.toString(a));
    }
}
