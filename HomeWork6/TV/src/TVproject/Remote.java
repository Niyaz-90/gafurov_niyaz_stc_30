package TVproject;


public class Remote {

    String[] channelList;
    String[] programList;

    private int channelCurrent;


    public Remote(String[] channelList, String[] programList){
        this.channelList = channelList;
        this.programList = programList;

    }

    public void changeChannel(int temp, Channel channel){
        this.channelCurrent = temp - 1;
       channel.getChannelInstance(channelList, channelCurrent, programList);
    }
}
