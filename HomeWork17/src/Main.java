import dao.DAO.CourseDaoImpl;
import dao.managers.CourseManager;
import dao.managers.LessonManager;
import dao.managers.TeacherManager;
import dao.models.Course;
import dao.models.Lesson;
import dao.models.Teacher;

import java.io.*;
import java.util.ArrayList;
import java.util.List;

public class Main {

    public static void main(String[] args) {
        //Курсы
        Course economics = new Course(1, "Economics", "24.11",
                "25.11", "Иванов_Петров", "Математика_Экономика");
        Course psychology = new Course(2, "Psychology", "26.11",
                "28.11", "Гафуров_Федотов", "Психология_Обществознание");
        Course physics = new Course(3, "Funny_Physics", "23.09",
                "12.10", "Артемьев_Гиззатов", "Физика_ Химия");

        //Учителя
        Teacher Ivanov = new Teacher("Иван", "Иванов", 12, "Economics");
        Teacher Petrov = new Teacher("Пётр", "Петров", 13, "Economics");
        Teacher Gafurov = new Teacher("Нияз", "Гафуров", 10, "Psychology");
        Teacher Fedotov = new Teacher("Фёдор", "Федотов", 15, "Psychology");
        Teacher Artemyev = new Teacher("Артём", "Артемьев", 9, "Funny_Physics");
        Teacher Gizzatov = new Teacher("Артур", "Гиззатов", 20, "Funny_Physics");

        //Уроки
        Lesson Maths = new Lesson("11:00", "12:00", "Математика", "Economics");
        Lesson Economics = new Lesson("13:00", "14:00", "Экономика", "Economics");
        Lesson Psychology = new Lesson("8:00", "9:30", "Психология", "Psychology");
        Lesson Social_studies = new Lesson("10:00", "11:30", "Обществознание", "Psychology");
        Lesson Physics = new Lesson("9:00", "10:00", "Физика", "Funny_Physics");
        Lesson Chemistry = new Lesson("17:00", "18:30", "Химия", "Funny_Physics");



        CourseManager courseManager = new CourseManager(new CourseDaoImpl());
        courseManager.addNewCourse(economics);
        courseManager.addNewCourse(physics);
        courseManager.addNewCourse(psychology);

        courseManager.findCourseByID(1);
        courseManager.findCourseByID(2);
        courseManager.findCourseByID(3);
        courseManager.findCourseByID(4);

        TeacherManager teacherManager = new TeacherManager(new CourseDaoImpl());
        teacherManager.addNewTeacher(Gafurov);
        teacherManager.addNewTeacher(Gizzatov);
        teacherManager.addNewTeacher(Ivanov);
        teacherManager.addNewTeacher(Fedotov);
        teacherManager.addNewTeacher(Petrov);
        teacherManager.addNewTeacher(Artemyev);

        System.out.println(teacherManager.findTeacherByFirstName("Нияз"));
        System.out.println(teacherManager.findTeacherByFirstName("Алексей"));
        System.out.println(teacherManager.findTeacherByFirstName("Иван"));

        LessonManager lessonManager = new LessonManager(new CourseDaoImpl());
        lessonManager.addNewLessonToCourse(Maths);
        lessonManager.addNewLessonToCourse(Economics);
        lessonManager.addNewLessonToCourse(Social_studies);

        System.out.println(lessonManager.findLessonByTitle("Экономика"));
        System.out.println(lessonManager.findLessonByTitle("Математика"));
        System.out.println(lessonManager.findLessonByTitle("Физкультура"));
    }
}
