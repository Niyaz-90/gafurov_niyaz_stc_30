package dao.models;

import java.util.List;

public class Course {
    int courseID;
    String courseNAme;
    String courseBegins;
    String courseEnds;
    public String teachersList;
    public String lessonsList;

    public Course(int courseID, String courseNAme, String courseBegins, String courseEnds,
                  String teachersList,  String lessonsList) {
        this.courseID = courseID;
        this.courseNAme = courseNAme;
        this.courseBegins = courseBegins;
        this.courseEnds = courseEnds;
        this.teachersList = teachersList;
        this.lessonsList = lessonsList;
    }

    public Course() {
    }

    public int getCourseID() {
        return courseID;
    }

    public String getCourseNAme() {
        return courseNAme;
    }

    public String getCourseBegins() {
        return courseBegins;
    }

    public String getCourseEnds() {
        return courseEnds;
    }

    public String getTeachersList() {
        return teachersList;
    } // переопределить метод: считать файл, преобразовать в стрингу

    public  String getLessonsList() {
        return lessonsList;
    }   // переопределить метод: считать файл, преобразовать в стрингу(у других классов, если надо, тоже сделать)



//    public void addLessons(Lesson lesson){
//        lessonsList.add(lesson);
//    }
//    public void addTeacher(Teacher teacher){
//        teachersList.add(teacher);
//    }

//    public Course setCourseEntity(){
//        return new Course(courseID,courseNAme,courseBegins,courseEnds, teachersList, lessonsList);
//    }
//
//    public String setCourseByString(){
//        return courseID + " " + courseNAme + " " + courseBegins + " " +
//                courseEnds + " " + teachersList + " " + lessonsList + "\r\n";
//    }

}
