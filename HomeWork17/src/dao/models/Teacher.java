package dao.models;

import java.util.ArrayList;
import java.util.List;

public class Teacher {
    private String teachersFirstName;
    private String teachersLastName;
    private int experience;
    private String courses;


    public Teacher(String teachersFirstName, String teachersLastName, int experience, String courses) {
        this.teachersFirstName = teachersFirstName;
        this.teachersLastName = teachersLastName;
        this.experience = experience;
        this.courses = courses;
    }

    public Teacher() {

    }

    public Teacher(String teachersFirstName, String teachersLastName) {

    }

    public String getTeachersFirstName() {

        return teachersFirstName;
    }

    public String getTeachersLastName() {
        return teachersLastName;
    }

    public int getExperience() {
        return experience;
    }

    public String getCourses() {
        return courses;
    }

    @Override
    public String toString() {
        return "Teacher: " + getTeachersFirstName() + " " +
               getTeachersLastName() + " " +
               getExperience() + " " +
               getCourses();
    }
//    public void addLessons(Lesson lesson){
//       lessons.add(lesson);
//    }
//
//
//    public void addCourses(Course course){
//        courses.add(course);
//    }

//    public List<Lesson> getLessonsList() {
//        return this.lessons;
//    }
//
//    public List<Course> getCorsesList() {
//        return this.courses;
//    }
}
