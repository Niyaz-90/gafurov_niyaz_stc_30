package dao.models;



import java.util.ArrayList;
import java.util.List;

public class Lesson {
    private String lessonStart;
    private String lessonFinish;
    private String lessonTitle;
    private String lessonOfCourse;


    public Lesson(String lessonStart, String lessonFinish, String lessonTitle, String lessonOfCourse) {
        this.lessonStart = lessonStart;
        this.lessonFinish = lessonFinish;
        this.lessonTitle = lessonTitle;
        this.lessonOfCourse = lessonOfCourse;
    }

    public Lesson() {

    }

    public String getLessonStart() {
        return lessonStart;
    }

    public String getLessonFinish() {
        return lessonFinish;
    }

    public String getLessonTitle() {
        return lessonTitle;
    }

    public String getLessonOfCourse() {
        return lessonOfCourse;
    }

    @Override
    public String toString() {
        return "Lesson{" +
                 getLessonStart() + " " +
                 getLessonFinish() + " " +
                 getLessonTitle() + " " +
                 getLessonOfCourse() + " " +
                '}';
    }

}
