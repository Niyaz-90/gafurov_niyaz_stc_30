package dao.DAO;

import dao.managers.CourseManager;
import dao.managers.LessonManager;
import dao.managers.TeacherManager;
import dao.models.Course;
import dao.models.Lesson;
import dao.models.Teacher;

import java.io.*;
import java.util.Optional;

public class CourseDaoImpl implements CourseDao, TeacherDAo, LessonDao{
    private CourseManager courseManager;
    private Teacher teacher;
    private Course course;
    private String fileName;
    private Lesson lesson;



    @Override
    public Lesson findByTitle(String title) {

        String currentLine;
        try {
            BufferedReader reader = new BufferedReader(new FileReader("lessons.txt"));
            while (true){
                currentLine = reader.readLine();
                if (currentLine == null){
                    System.out.println("Некорректное название урока");
                    return null;
                }
                String[] data = currentLine.split(" ");
                if (data[2].equals(title)){
                    return stringToLessonMapper.map(currentLine);
                }
            }



        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }



    @Override
    public String findByID(int iD) {
        try {
            BufferedReader reader = new BufferedReader(new FileReader("course.txt"));
            String current;
            while (reader.readLine() != null){
                current = reader.readLine();
                if(current == null){
                    System.out.println(iD + " " + " - Некорректный ID курса");
                    return null;
                }
                String[] data = current.split(" ");
                if(Integer.parseInt(data[0]) == iD){
                    System.out.println(current);
                    return current;
                }
            }
            return null;
        } catch (FileNotFoundException e) {
            throw new IllegalStateException(e);
        } catch (IOException e) {
            throw new IllegalArgumentException(e);
        }
    }
    public Mapper<Course, String> courseToStringMapper = course ->
            course.getCourseID() + " " +
                    course.getCourseNAme() + " " +
                    course.getCourseBegins() + " " +
                    course.getCourseEnds() + " " +
                    course.getTeachersList() + " " +
                    course.getLessonsList() + "\r\n";

    public Mapper<String, Course> stringToCourseMapper = course -> {
        String[] data = course.split(" ");
        return new Course(Integer.parseInt(data[0]), data[1], data[2], data[3],
                 data[4], data[5]);
    };

    @Override
    public void save(Course entity) {

        try {
            BufferedOutputStream outputStream =
                    new BufferedOutputStream(new FileOutputStream("course.txt", true));
            outputStream.write(courseToStringMapper.map(entity).getBytes());
            outputStream.close();

        } catch (FileNotFoundException e) {
            throw new IllegalArgumentException(e);
        } catch (IOException e) {
            throw new IllegalArgumentException(e);
        }
    }

    @Override
    public void save(Teacher entity) {

        try {
            BufferedOutputStream outputStream = new BufferedOutputStream(new FileOutputStream("teachersInfo.txt", true));
            outputStream.write(teacherToStringMapper.map(entity).getBytes());
            outputStream.close();

        } catch (FileNotFoundException e) {
            throw new IllegalArgumentException(e);
        } catch (IOException e) {
            throw new IllegalArgumentException(e);
        }
    }

    public Mapper<Teacher, String> teacherToStringMapper = teacher ->
            teacher.getTeachersFirstName() + " " +
                    teacher.getTeachersLastName() + " " +
                    teacher.getExperience() + " " +
                    teacher.getCourses() + "\r\n";

    public Mapper<String, Teacher> stringToTeacherMapper = teacher -> {
        String[] data = teacher.split(" ");
        return new Teacher(data[0], data[1], Integer.parseInt(data[2]), data[3]);
    };

    @Override
    public Teacher findByFirstName(String name) {
        String currentLine = "";
        try {
            BufferedReader reader = new BufferedReader(new FileReader("teachersInfo.txt"));
            while (true){
                currentLine = reader.readLine();
                if (currentLine == null){
                    System.out.println("Учителя, с таким именем, не найденo");
                    return null;
                }
                String[] data = currentLine.split(" ");
                if (data[0].equals(name)){
                    return stringToTeacherMapper.map(currentLine);
                }
            }
//            System.out.println("Учителя, с таким именем, не найденo");

//            return null;

        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            throw new IllegalStateException(e);
        }
        return null;
    }

    @Override
    public void save(Lesson entity) {

        try {
            BufferedOutputStream outputStream = new BufferedOutputStream(new FileOutputStream("lessons.txt", true));
            outputStream.write(lessonToStringMapper.map(entity).getBytes());
            outputStream.close();

        } catch (FileNotFoundException e) {
            throw new IllegalArgumentException(e);
        } catch (IOException e) {
            throw new IllegalArgumentException(e);
        }
    }

    private Mapper<Lesson, String> lessonToStringMapper = lesson ->
            lesson.getLessonStart() + " " +
                    lesson.getLessonFinish() + " " +
                    lesson.getLessonTitle() + " " +
                    lesson.getLessonOfCourse() + "\r\n";
    private Mapper<String, Lesson> stringToLessonMapper = lesson -> {
        String[] data = lesson.split(" ");
        return new Lesson(data[0] , data[1] , data[2] , data[3]);
    };
}


