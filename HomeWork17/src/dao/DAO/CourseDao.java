package dao.DAO;

import dao.models.Course;
import dao.models.Lesson;
import dao.models.Teacher;

public interface CourseDao {

    String findByID(int iD);
    void save(Course entity);
}
