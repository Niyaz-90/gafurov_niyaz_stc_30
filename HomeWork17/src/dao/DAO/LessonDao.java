package dao.DAO;

import dao.models.Lesson;

public interface LessonDao {
    Lesson findByTitle(String title);
    void save(Lesson entity);
}
