package dao.DAO;

import dao.models.Teacher;

public interface TeacherDAo {
    Teacher findByFirstName(String name);
    void save(Teacher entity);
}
