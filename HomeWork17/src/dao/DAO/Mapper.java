package dao.DAO;

public interface Mapper<X, Y> {
    Y map(X x);
}
