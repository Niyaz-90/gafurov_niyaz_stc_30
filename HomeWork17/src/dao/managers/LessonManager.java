package dao.managers;

import dao.DAO.CourseDaoImpl;
import dao.DAO.LessonDao;
import dao.DAO.Mapper;
import dao.models.Lesson;
import dao.models.Teacher;

import java.io.*;

public class LessonManager {
    private CourseDaoImpl courseDao;
    private Lesson lesson;

    public LessonManager(String startTime, String finishTime, String title, String lessonsOfCourse) {
        this.lesson = new Lesson(startTime, finishTime, title, lessonsOfCourse);
    }

    public LessonManager() {
    }

    public LessonManager(CourseDaoImpl courseDao) {
        this.courseDao = courseDao;
    }

    public void addNewLessonToCourse(Lesson lesson){
        courseDao.save(lesson);
    }

    public String findLessonByTitle(String lessonTitle){
        Lesson tempLesson = courseDao.findByTitle(lessonTitle);
        if (tempLesson == null){
            return null;
        }
        this.lesson = tempLesson;

        return lesson.toString();

    }




}
