package dao.managers;

import dao.DAO.CourseDaoImpl;
import dao.DAO.Mapper;
import dao.DAO.TeacherDAo;
import dao.models.Course;
import dao.models.Teacher;

import java.io.*;

public class TeacherManager {
    private CourseDaoImpl courseDao;
    private Teacher teacher = new Teacher();

    public TeacherManager(String teachersFirstName, String teachersLastName, int experience, String courses) {
        this.teacher = new Teacher(teachersFirstName, teachersLastName, experience, courses);
    }

    public TeacherManager() {
    }

    public TeacherManager(CourseDaoImpl courseDao) {
        this.courseDao = courseDao;
    }

    public void addNewTeacher(Teacher teacher){
        courseDao.save(teacher);
    }

    public String findTeacherByFirstName(String name){
        Teacher tempTeacher = courseDao.findByFirstName(name);
        if (tempTeacher == null){
            return null;
        }
        teacher = tempTeacher;

        return tempTeacher.toString();
    }


}
