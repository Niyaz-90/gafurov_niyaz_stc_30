package dao.managers;

import dao.DAO.CourseDao;
import dao.DAO.CourseDaoImpl;
import dao.DAO.Mapper;
import dao.DAO.TeacherDAo;
import dao.models.Course;
import dao.models.Lesson;
import dao.models.Teacher;

import java.io.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class CourseManager  {
    private CourseDaoImpl courseDao;
    private Course course = new Course();

    public CourseManager(int courseID, String courseNAme, String courseBegins, String courseEnds,
                         String teachersList,  String lessonsList) {
        this.course = new Course(courseID, courseNAme, courseBegins, courseEnds, teachersList, lessonsList);
    }


    public CourseManager(CourseDaoImpl courseDao) {
        this.courseDao = courseDao;
    }

    public void addNewCourse(Course course){
        courseDao.save(course);
    }

    public String findCourseByID(int iD) {
        String result = courseDao.findByID(iD);
        if (result == null){
            System.out.println("Некорректное название урока");
            return null;
        }

        return result;
    }



}
