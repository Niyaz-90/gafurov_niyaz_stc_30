import java.util.Arrays;

public class Main {

    public static void quickSort(int low, int high, String[] array){
        int midlle = low + (high - low) / 2 ;
        String pivot = array[midlle];
        if (array.length == 0){
            return ;
        }
        if (low >= high){
            return ;
        }
        int i = low;
        int j = high;
        while (i <= j){
            while (array[i].compareTo(pivot) < 0){
                i++;
            }
            while (array[j].compareTo(pivot) > 0){
                j--;
            }
            if(i <= j){
                String temp = array[i];
                array[i] = array[j];
                array[j] = temp;
                i++;
                j--;
            }


        }
        if (low < j){
            quickSort(low, j, array);
        }
        if (i < high){
            quickSort(i, high, array);
        }

    }

    public static void main(String[] args) {
        String[] array = {"алфавит", "глобус", "авто", "газ", "пальто", "дорога", "мышь", "молоток", "молот", "компьютер",
                "Интернет", "интернет"};
        System.out.println(Arrays.toString(array));
        int low = 0;
        int high = array.length - 1;
        quickSort(low, high, array);
        System.out.println(Arrays.toString(array));

    }
}
