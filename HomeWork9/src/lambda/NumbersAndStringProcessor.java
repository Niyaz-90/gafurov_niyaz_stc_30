package lambda;

public class NumbersAndStringProcessor  {

    public String[] textArray;
    public int[] numberArray;
    public int number;
    public String text;


    public NumbersAndStringProcessor(int[] numberArray, String[] textArray) {
        this.numberArray = numberArray;
        this.textArray = textArray;

    }

    public String[] processString(StringsProcessor stringProcess){
        for (int i = 0; i < textArray.length; i++){

            textArray[i] = stringProcess.processsStrings(textArray[i]);

//            return textArray[i];
        }

        return  textArray;

    }

    public int[] processNumber(NumbersProcessor numberProcess){
        for (int i = 0; i < numberArray.length; i++){
//            number = numberArray[i];
            numberArray[i] = numberProcess.processNumber(numberArray[i]);



        }
        return   numberArray;
    }


}
