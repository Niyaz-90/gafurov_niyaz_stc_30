package lambda;

import java.util.Arrays;

public class Main {

    public static void main(String[] args) {
        int[] numbersArr = {900, 567, 900, 502, 3008, 5070, 7504};
        String[] stringsArray = {"sh sfv24idf", "fkdhd93 rh458 ekfd", "dhvf094d dfdf", "fdjh9586 7969jgj", "fjv9458f uj"};



        NumbersAndStringProcessor processor = new NumbersAndStringProcessor(numbersArr, stringsArray);

        NumbersProcessor numbersProcess = number -> {
//        String numberInString = String.valueOf(number);
        int count = 0;
        int[] numberArray = new int[String.valueOf(number).length()];
        for (int i = 0; i < numberArray.length; i++) {
            numberArray[i] = number % 10;
            number = number / 10;
            if (numberArray[i] != 0) {
                count++;
            }
        }

        int[] tempArray = new int[count];
        int j = 0;

        for (int i = 0; i < numberArray.length; i++) {
            if (numberArray[i] > 0) {
                tempArray[j] = numberArray[i];
                j++;
            }
        }

        for (int i = 0; i < tempArray.length; i++) {
            if (tempArray[i] % 2 != 0) {
                tempArray[i] = tempArray[i] - 1;
            } else {
                tempArray[i] = tempArray[i];
            }
        }
//
        String resultNumber = "";


        for (int i = 0; i < tempArray.length; i++){

            resultNumber += tempArray[i];
        }
        return Integer.parseInt(resultNumber);
            };




        StringsProcessor stringsProcess = text -> {
                int count = 0;
                char[] lineAsCharArray = text.toCharArray();
                char[] reversLineAsCharArray = new char[lineAsCharArray.length];
                int j = 0;
                for(int i = lineAsCharArray.length - 1; i >= 0; i--) {


                        reversLineAsCharArray[j] = lineAsCharArray[i];
                        j++;
                        if(Character.isDigit(lineAsCharArray[i])){
                            count++;
                        }
                }
                char[] withoutNumbersArray = new char[reversLineAsCharArray.length - count];

                int k = 0;
                for (int i = 0; i < reversLineAsCharArray.length; i++){

                    if(Character.isLetter(reversLineAsCharArray[i])){
                        withoutNumbersArray[k] = reversLineAsCharArray[i];
                        k++;
                    } else if (reversLineAsCharArray[i] == ' '){
                        withoutNumbersArray[k] = ' ';
                        k++;
                    }
                }
                for(int i = 0; i < withoutNumbersArray.length; i++){

                    withoutNumbersArray[i] = Character.toUpperCase(withoutNumbersArray[i]);
                }

                return String.copyValueOf(withoutNumbersArray);
            };


        System.out.println(Arrays.toString(processor.processNumber(numbersProcess)));
        System.out.println(Arrays.toString(processor.processString(stringsProcess)));

    }
}
