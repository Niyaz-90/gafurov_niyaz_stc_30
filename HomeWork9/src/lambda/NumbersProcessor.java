package lambda;

public interface NumbersProcessor {
    int processNumber(int number);
}
