package program.models;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor

public class CurrencyOption {
    private String optionName;
    private double volatility;
    private double call_delta;
    private double call_settle;
    private double call;
    private double strike;
    private double put;
    private double put_settle;
    private double put_delta;
    private double straddle;
    private double gamma;
    private double vega;
    private double theta;
}
