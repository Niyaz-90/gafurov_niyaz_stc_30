package program.server;

import program.data.*;

import java.io.*;
import java.net.ServerSocket;
import java.net.Socket;

public class Server {

    private Socket client;
    private ClientThread userThread;
    private DataGetter dataGetter = new DataGetterImpl();
    private DataProcessor dataProcessor;
    private DataSetter dataSetter;
    private static final String FUTURES = "Euro FX Futures, Japanese Yen Futures, British Pound Futures," +
            " Canadian Dollar Futures, Australian Dollar Futures," ;


    public void start(int port) {
        try {
            ServerSocket serverSocket = new ServerSocket(port);
            if (client == null) {

                client = serverSocket.accept();
                userThread = new ClientThread(client);
                userThread.toUser.println("Вы подключились к серверу");
                dataGetter.getJsonTable();
//                dataGetter.getOptionTable();
                userThread.start();

            } else {
                System.out.println("Превышено количество допустимых пользователей");
            }
        } catch (IOException e) {
            throw new IllegalStateException(e);
        }
    }

    public class ClientThread extends Thread {
        private Socket user;
        private String userName;
        private String userIP;
        private PrintWriter toUser;
        private BufferedReader fromUser;

        public ClientThread(Socket user) {
            this.user = user;
            try {
                this.toUser = new PrintWriter(user.getOutputStream(), true);
                this.fromUser = new BufferedReader(new InputStreamReader(user.getInputStream()));
            } catch (IOException e) {
                throw new IllegalArgumentException(e);
            }
        }


        @Override
        public void run() {

            while (true){

                try {
                    String userMessage = fromUser.readLine();


                    if (userMessage.equals("go")){

                        String[] splitted = FUTURES.split(",");
                        dataSetter = new DataSetterImpl();


                        for (String future : splitted) {
                            dataProcessor = new DataProcessorImpl(future);
                            dataSetter.put(dataProcessor.structuringOptionData(), dataProcessor.breakPointProcessor());
                            System.out.println("That's all");
                            toUser.println("exit");
                            System.exit(0);


                        }
                    } else {
                        System.out.println("exit");
                        System.exit(-1);
                    }
                } catch (IOException e) {
                    throw new IllegalStateException(e);
                }
            }


        }
    }
}
