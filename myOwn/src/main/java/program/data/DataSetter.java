package program.data;

import program.models.BreakPoint;
import program.models.CurrencyOption;

public interface DataSetter {
    void put(CurrencyOption option, BreakPoint forwardPoints);

}
