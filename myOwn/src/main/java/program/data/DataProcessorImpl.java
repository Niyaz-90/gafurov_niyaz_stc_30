package program.data;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.select.Elements;
import program.models.BreakPoint;
import program.models.CurrencyOption;

import java.io.*;
import java.time.LocalDate;

public class DataProcessorImpl implements DataProcessor {
    private static final String JSON_FILE = "C:/Users/niyaz/Git/json's/CME_tempData_" + LocalDate.now() + "_.json";
    private static final String HTML_FILE_SRC_EUR = "C:/Users/niyaz/Git/json's/html's/EUR.html";
    private static final String HTML_FILE_SRC_GBP = "C:/Users/niyaz/Git/json's/html's/GBP.html";
    private static final String HTML_FILE_SRC_AUD = "C:/Users/niyaz/Git/json's/html's/AUD.html";
    private static final String HTML_FILE_SRC_JPY = "C:/Users/niyaz/Git/json's/html's/JPY.html";
    private static final String HTML_FILE_SRC_CHF = "C:/Users/niyaz/Git/json's/html's/CHF.html";
    private static final String HTML_FILE_SRC_NZD = "C:/Users/niyaz/Git/json's/html's/NZD.html";
    private static final String HTML_FILE_SRC_CAD = "C:/Users/niyaz/Git/json's/html's/CAD.html";
    private static final String HTML_FILE_PROCESSED_DATA = "C:/Users/niyaz/Git/json's/QuikStrike_tempData_" + LocalDate.now() + "_.csv";
    private BreakPoint currencyFuture;
    private CurrencyOption currencyOption;
    private File fileForJsoup;
    private String currency;

    public DataProcessorImpl(String currency) {
        this.currency = currency;
    }

    @Override
    public BreakPoint breakPointProcessor() {

        currencyFuture = new BreakPoint();
        int count = 0;
        try {
            Reader reader = new FileReader(JSON_FILE);


            JSONParser json = new JSONParser();
            JSONArray obj = (JSONArray) json.parse(reader);
            JSONObject currencyObject = (JSONObject) obj.get(0);


            while (!currencyObject.get("productName").equals(this.currency)) {

                if (currencyObject.get("productName").equals(this.currency)) {

                    currencyFuture.setCurrencyName(this.currency + "_Future");
                    currencyFuture.setOpenPriceFuture(Double.parseDouble(currencyObject.get("open").toString()));
                    String open = String.valueOf(currencyFuture.getOpenPriceSpot());
                    String last = currencyObject.get("last").toString();
                    System.out.println(this.currency + " last: " + last);
                    System.out.println();
                    System.out.println(this.currency + " open: " + open);
                    System.out.println();
                    return BreakPoint.builder()
                            .CurrencyName(this.currency)
                            .openPriceFuture(Double.parseDouble(currencyObject.get("open").toString()))
                            .openPriceSpot(0.0)
                            .build();


                } else {
                    count++;
                    currencyObject = (JSONObject) obj.get(count);
                }
            }
            reader.close();
        } catch (FileNotFoundException e) {
            throw new IllegalStateException("Cannot read file");
        } catch (ParseException e) {
            throw new IllegalArgumentException("Cannot parse JSON file(Array)");
        } catch (IOException e) {
            throw new IllegalArgumentException("Cannot parse JSON file(Array) (IO ex)");
        }
        return null;
    }

    @Override
    public CurrencyOption structuringOptionData() {
//        currencyOption = new CurrencyOption();

        fileForJsoup = new File("C:/Users/niyaz/Git/json's/html's/" + this.currency + ".html");
//        currencyOption.setOptionName(this.currency + "_Future");
        try {
            Document html = Jsoup.parse(fileForJsoup, "UTF-8");

            FileWriter htmlFile = new FileWriter(HTML_FILE_PROCESSED_DATA, true);
            Elements trATM = html.select("table#pricing-sheet.grid-thm.base-sheet.w-lg.grid-thm-v2").select("tr.currentATM");
            htmlFile.write(this.currency + trATM.text());

            String[] fromHtml = trATM.text().split(" ");

            for (int i = 0; i < fromHtml.length; i++) {
                fromHtml[i] = fromHtml[i].replace(",", ".");
            }
//            forConvert = forConvert.replaceAll(",", ".");
//                if (forConvert.contains(",")){
//                    char[] convert = forConvert.toCharArray();
//                    for (int i = 0; i < convert.length; i++){
//                        if (convert[i] == ','){
//                            convert[i] = '.';
//                        }
//                    }
//                }
//            }



//            currencyOption.setVolatility(Double.parseDouble(fromHtml[0]))
//            currencyOption.setCall_delta(Double.parseDouble(fromHtml[1]));
//            currencyOption.setCall_settle(Double.parseDouble(fromHtml[2]));
//            currencyOption.setCall(Double.parseDouble(fromHtml[3]));
//            currencyOption.setStrike(Double.parseDouble(fromHtml[4]));
//            currencyOption.setPut(Double.parseDouble(fromHtml[5]));
//            currencyOption.setPut_settle(Double.parseDouble(fromHtml[6]));
//            currencyOption.setPut_delta(Double.parseDouble(fromHtml[7]));
//            currencyOption.setStraddle(Double.parseDouble(fromHtml[8]));
//            currencyOption.setGamma(Double.parseDouble(fromHtml[9]));
//            currencyOption.setVega(Double.parseDouble(fromHtml[10]));
//            currencyOption.setTheta(Double.parseDouble(fromHtml[11]));



            htmlFile.close();
            return  CurrencyOption.builder()
                    .optionName(this.currency )
                    .volatility(Double.parseDouble(fromHtml[0]))
                    .call_delta(Double.parseDouble(fromHtml[1]))
                    .call_settle(Double.parseDouble(fromHtml[2]))
                    .call(Double.parseDouble(fromHtml[3]))
                    .strike(Double.parseDouble(fromHtml[4]))
                    .put(Double.parseDouble(fromHtml[5]))
                    .put_settle(Double.parseDouble(fromHtml[6]))
                    .put_delta(Double.parseDouble(fromHtml[7]))
                    .straddle(Double.parseDouble(fromHtml[8]))
                    .gamma(Double.parseDouble(fromHtml[9]))
                    .vega(Double.parseDouble(fromHtml[10]))
                    .theta(Double.parseDouble(fromHtml[11]))
                    .build();

        } catch (IOException e) {
            e.printStackTrace();
        }

        return null;
    }

}
