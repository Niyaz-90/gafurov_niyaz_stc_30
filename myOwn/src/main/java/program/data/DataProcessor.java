package program.data;

import program.models.BreakPoint;
import program.models.CurrencyOption;

public interface DataProcessor {
    BreakPoint breakPointProcessor();
    CurrencyOption structuringOptionData();
}
