package program.data;

public interface DataGetter {
    void getJsonTable();
    void getOptionTable();
}
