package nested;

import java.util.Scanner;

public class BuilderPrimitive {
    Scanner scanner = new Scanner(System.in);

    private  static String courseListener;

    private final int age;
    private final String firstName;
    private final String lastname;
    private final boolean isFinishedCourse;


    public String toString() {
        return (" name = " + this.firstName + " " + this.lastname + " age = " + this.age + " "
                + "course status = " + this.isFinishedCourse);
    }

    public static class Builder {

         final String courseListener ;

         int age ;
         String firstName;
         String lastName;
         boolean isFinishedCourse;

        public Builder(String courseListener){

            this.courseListener = courseListener;
        }


        public Builder setAge(int value){
            age = value;
            return  this;
        }

        public Builder setFirstName(String value) {
            firstName = value;
            return this;
        }
        public Builder setLastName(String value) {
            lastName = value;
            return this;
        }

        public Builder setIsFinishedCourse(boolean value){
            isFinishedCourse = value;
            return this;
        }

        public BuilderPrimitive build(){
            return new BuilderPrimitive(this);
        }

    }
    protected BuilderPrimitive(Builder builder){
        age = builder.age;
        firstName = builder.firstName;
        lastname = builder.lastName;
        isFinishedCourse = builder.isFinishedCourse;


    }



}
