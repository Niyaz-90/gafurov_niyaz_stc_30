package nested;

public class Main {


    public static void main(String[] args) {

        BuilderPrimitive builderPrimitive = new BuilderPrimitive.Builder("yes")
                .setAge(34)
                .setFirstName("Gafurov")
                .setLastName("Niyaz")
                .setIsFinishedCourse(true)
                .build();
        BuilderPrimitive builderPrimitive1 = new BuilderPrimitive.Builder("yes")
                .setAge(36)
                .setFirstName("Ivanov")
                .setLastName("Fedor")
                .setIsFinishedCourse(false)
                .build();

        System.out.println(builderPrimitive);
        System.out.println(builderPrimitive1);

    }
}
