package generics;

public interface List<D> extends Collection<D> {
    D get(int index);
    int indexOf(D element);
    void remove(int index);
    void insert(D element, int index);

}
