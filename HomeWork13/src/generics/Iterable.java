package generics;

public interface Iterable<B> {
    Iterator<B> iterator();
}
