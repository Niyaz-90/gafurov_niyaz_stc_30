package generics;


public class MainArrayList {

    public static void main(String[] args) {


        List<Integer> list = new ArrayList<>();


        for (int i = 1; i < 9; i++) {
            list.add(i);
        }
        System.out.println("get list(2) " + list.get(2));
        System.out.println("get list(6) " + list.get(6));
        System.out.println("get index of 8 " + list.indexOf(8));
        list.add(67);
        list.add(65);
        list.add(785);
        list.add(710);
        list.add(129);
        System.out.println("get index of 67 " + list.indexOf(67));
        System.out.println("get index of 65 " + list.indexOf(65));
        System.out.println("get index of 785 " + list.indexOf(785));
        System.out.println("get index of 710 " + list.indexOf(710));
        System.out.println("get index of 129 " + list.indexOf(129));
//        System.out.println("contains (5) " + list.contains(5));
//        System.out.println("contains (5) " + list.contains(2));
//        list.insert(163, 2);
//        list.remove(3);
        System.out.println("size " + list.size());
//        System.out.println("contains (5) " + list.contains(163));
//        System.out.println("get index of 0 " + list.get(0));
//        System.out.println("get index of 163 " + list.indexOf(163));






        Iterator iterator = list.iterator();
        while (iterator.hasNext()){
            System.out.println("--> " + iterator.next());

        }

        System.out.println(list.toString());


    }
}
