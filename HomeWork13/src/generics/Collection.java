package generics;

public interface Collection<C> extends Iterable<C> {
    void add(C element);
    boolean contains(C element);
    int size();
//    void remove(C element);
}
