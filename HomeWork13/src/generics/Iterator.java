package generics;

public interface Iterator<A> {
    A next();
    boolean hasNext();
}
