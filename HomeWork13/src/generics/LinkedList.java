package generics;

import java.util.NoSuchElementException;

public class LinkedList<F> implements List<F> {
    private Node<F> first;
    private Node<F> last;
    private int count;

    private class LinkedListIterator implements Iterator<F> {
        LinkedListIterator linkedListIterator;
        private int currentIndex = 0;
        Node<F> current;
        Node<F> nextCurrent = first;

        @Override
        public F next() {
                while (hasNext()) {
                    current = nextCurrent;
                    nextCurrent = current.next;
                    this.currentIndex++;
                    return current.value;
                }
             throw new NoSuchElementException();

        }

        @Override
        public boolean hasNext() {
            return currentIndex < count;
        }
    }

    @Override
    public Iterator<F> iterator() {
        return new LinkedListIterator();
    }

    public static class Node<G>{
        G value;
        Node<G> next;

        public Node(G value) {
            this.value = value;
        }
        
    }


    @Override
    public F get(int index) {
        if(index >= 0 && index < count && first != null) {
            int i = 0;

            Node<F> current = this.first;
            while (i < index) {
                current = current.next;
                i++;
            }
            return current.value;
        } else {
            System.out.print(" Элемент не найден ");
            return null;
        }

    }

    @Override
    public int indexOf(F element) {
        Node<F> current = this.first;
        int i = 0;

        while(current != null && !current.value.equals(element)){
            current = current.next;
            i++;
        }

        if (current == null) {
            System.out.print("Такого элемента нет: ");
            return -1;
        } else {
            return i;
        }
    }

    @Override
    public void add(F element) {

        Node<F> newNode = new Node<>(element);
        if (first == null){
            first = newNode;
            last = newNode;
        }else {
            last.next = newNode;
            last = newNode;
        }
        count++;

    }

    @Override
    public boolean contains(F element) {
        if(indexOf(element) != -1){
            return true;
        } else
        return false;
    }

    @Override
    public int size() {
        return count;
    }

    @Override
    public void remove(int index) {
        Node<F> current = first;
        Node<F> nextCurrent = first;
        if(get(index) != null){
            int i = 0;
            while (i < index){
                current = nextCurrent;
                nextCurrent = current.next;
                i++;
            }
            int j = 0;
            while (j < count - 2) {
                current.next = nextCurrent.next;
                j++;
            }
            count--;
        }

        else {
            System.err.println("Index not found");
        }



    }

    @Override
    public void insert(F element, int index) {

        Node<F> current = first;
        Node<F> nextCurrent = first;
        Node<F> nextAfterNew;
        if(first != null){
            int i = 0;
            while (i != index + 1) {
                current = nextCurrent;
                nextCurrent = current.next;
                i++;
            }
            nextAfterNew = current.next;

            Node<F> newNode = new Node<>(element);
            current.next = newNode;



            newNode.next = nextAfterNew;
            newNode.value = element;


            this.count++;


        }


    }

    @Override
    public String toString() {
        StringBuilder stringBuilder = new StringBuilder();
        Iterator<F> iterator = this.iterator();
        while (iterator.hasNext()){
            stringBuilder.append(iterator.next()).append(" ");
        }
        return "LinkedList{ " + stringBuilder.toString() + "}";
    }
}
