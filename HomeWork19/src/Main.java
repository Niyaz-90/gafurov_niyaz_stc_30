import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.Map;
import java.util.function.Function;
import java.util.stream.Collectors;

public class Main {

    public static void main(String[] args) {

        System.out.println();

        System.out.print("Вывод всех авто с нулевым пробегом, чёрного цвета: ");
        try {
            Files.lines(Path.of("carsInfo.txt"))
                    .map(line -> line.split(" "))
                    .map(data -> new CarInfo(Integer.parseInt(data[0]), data[1], data[2],
                            Integer.parseInt(data[3]), Integer.parseInt(data[4])))
                    .filter(carInfo -> carInfo.getMileage() == 0 | carInfo.getColor().equals("black"))
                    .forEach(System.out::println);
        } catch (IOException e) {
            throw new IllegalStateException(e);
        }
        System.out.println("---------------------");


        System.out.print("Результат первого варианта реализации: ");
        try {
           int carsToPrint =  Files.lines(Path.of("carsInfo.txt"))
                    .map(lines -> lines.split(" "))
                    .map(data -> new CarInfo(data[1], Integer.parseInt(data[4])))
                    .collect(Collectors.toList())

                    .stream()
                    .filter(carInfo -> carInfo.getPrice() < 800000 & carInfo.getPrice() > 700000)
                    .map(CarInfo::getCarModel)
                    .collect(Collectors.toSet())
                   .size();
            System.out.println(carsToPrint);
        } catch (IOException e) {
            throw new IllegalArgumentException(e);
        }

        System.out.print("Результат второго варианта реализации: ");
        try {
            int uniqueCars = (int) Files.lines(Path.of("carsInfo.txt"))
                    .map(line -> line.split(" "))
                    .filter(line -> Integer.parseInt(line[4]) > 700000 & Integer.parseInt(line[4]) < 800000)
                    .map(line -> line[1])
                    .distinct()
                    .count();
            System.out.println(uniqueCars);

        } catch (IOException e) {
            e.printStackTrace();
        }

        System.out.println("---------------------");



        System.out.print("Цвет самой дешёвого авто: ");
        try {
            String chipestColor = Files.lines(Path.of("carsInfo.txt"))
                    .map(line -> line.split(" "))

                    .map(line -> new CarInfo( line[1], line[2],
                             Integer.parseInt(line[4])))

                    .min(Comparator.comparing(CarInfo::getPrice))
                    .map(carInfo -> carInfo.getColor()).get();
            System.out.println(chipestColor);


        } catch (IOException e) {
            throw new IllegalArgumentException(e);
        }


        System.out.println("---------------------");

        System.out.print("Средняя цена Camry: ");

        double camryAveragePrice = 0;
        try {
            camryAveragePrice = Files.lines(Path.of("carsInfo.txt"))
                    .map(line -> line.split(" "))
                    .filter(line -> line[1].equals("Camry"))

                    .mapToInt(line -> Integer.parseInt(line[4]))
                    .average()
                    .getAsDouble();
        } catch (IOException e) {
            throw new IllegalStateException(e);
        }
        System.out.println(camryAveragePrice);


    }
}
