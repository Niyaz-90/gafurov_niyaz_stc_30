public class CarInfo {
    private int carID;
    private String carModel;
    private String color;
    private int mileage;
    private int price;

    public CarInfo(int carID, String carModel, String color, int mileage, int price) {
        this.carID = carID;
        this.carModel = carModel;
        this.color = color;
        this.mileage = mileage;
        this.price = price;
    }

    public CarInfo(String carModel, int price) {
        this.carModel = carModel;
        this.price = price;
    }

    public CarInfo(String carModel, String color, int price) {
        this.carModel = carModel;
        this.color = color;
        this.price = price;
    }

    public int getCarID() {
        return carID;
    }

    public String getCarModel() {
        return carModel;
    }

    public String getColor() {
        return color;
    }

    public int getMileage() {
        return mileage;
    }

    public int getPrice() {
        return price;
    }

    @Override
    public String toString() {
        return "CarInfo{" +
                "carID=" + carID +
                ", carModel='" + carModel + '\'' +
                ", color='" + color + '\'' +
                ", mileage=" + mileage +
                ", price=" + price +
                '}';
    }
}
