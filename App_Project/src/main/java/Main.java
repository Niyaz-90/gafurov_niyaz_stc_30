import com.zaxxer.hikari.HikariConfig;
import com.zaxxer.hikari.HikariDataSource;
import inno.models.Game;
import inno.models.Player;
import inno.repositories.GameRepository;
import inno.repositories.GameRepositoryImpl;
import inno.repositories.PlayerRepository;
import inno.repositories.PlayerRepositoryImpl;
import inno.server.GameServer;

import java.net.Inet4Address;
import java.net.UnknownHostException;
import java.sql.Time;
import java.time.LocalDateTime;
import java.util.Calendar;
import java.util.Date;
import java.util.Properties;

public class Main {
    public static void main(String[] args) throws UnknownHostException {



            GameServer gameServer = new GameServer();
            gameServer.start(7777);
        }
}
