package inno.repositories;

import inno.models.Player;

public interface PlayerRepository extends CRUDRepository<Player, Long> {
    @Override
    void save(Player entity);
//    void update( Long id);
     void update(Player entity);
    Player findByID(Long id);
    Player findByNickName(String nickName);
    void updateID(Player player);
}
