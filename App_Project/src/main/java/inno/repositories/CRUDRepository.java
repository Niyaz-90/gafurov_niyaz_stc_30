package inno.repositories;

import com.sun.xml.internal.bind.v2.model.core.ID;

import java.util.Optional;


public interface CRUDRepository<T, ID> {
    void save(T entity);

}
