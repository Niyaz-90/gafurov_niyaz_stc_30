package inno.repositories;

import inno.models.Game;
import inno.models.Player;

import javax.sql.DataSource;
import java.sql.*;
import java.util.Date;

public class GameRepositoryImpl implements GameRepository {
    private Date dateOfGame;
    private int gameDuration;

//    Statement statement = null;
    //language=sql
    private static final String SQL_INSERT = "insert into games(begins, player1_id, player2_id) " +
            "values (?, ?, ?); ";

    //language=sql
    private static final String SQL_INSERT_PLAYERS_IN_GAME = "update games " +
            "set player1_id = ?, player2_id = ? where  game_id = ?; ";

    //language=sql
    public static final String SQL_UPDATE = "update games " +
            "set begins = ?, player1_id = ?, player1_shots = ?," +
            "player2_id = ?, player2_shots = ?, " +
            "game_duration = ?  where game_id = ?" +
            ";";

    private DataSource dataSource;

    public GameRepositoryImpl(DataSource dataSource) {
        this.dataSource = dataSource;
    }


    @Override
    public void save(Game entity) {
        try (Connection connection = dataSource.getConnection();
        PreparedStatement preparedStatement = connection.prepareStatement(SQL_INSERT, Statement.RETURN_GENERATED_KEYS)){
            preparedStatement.setString(1, entity.getDate());
            preparedStatement.setLong(2, entity.getFirstPlayer().getPlayerID());
            preparedStatement.setLong(3, entity.getSecondPlayer().getPlayerID());

            int insertRowsCount = preparedStatement.executeUpdate();
            if (insertRowsCount == 0){
                System.out.println("Cannot save entity");
            }

            ResultSet rows = preparedStatement.getGeneratedKeys();
            if (rows.next()){
                long generatedId = rows.getLong("game_id");
                entity.setID(generatedId);
//                PreparedStatement preparedStatement1 = connection.prepareStatement(SQL_INSERT_PLAYERS_IN_GAME);
//                preparedStatement1.setLong(1, generatedId);
            }
            else {
                throw new IllegalStateException("Cannot return generated key");
            }
            rows.close();
        }
        catch (SQLException e){
            throw new IllegalStateException(e);
        }
    }

    @Override
    public Game findByID(Long id) {
        try(Connection connection = dataSource.getConnection();
        PreparedStatement preparedStatement = connection.prepareStatement("select * from games where game_id = ?")){
            preparedStatement.setLong(1, id);


            ResultSet resultSet = preparedStatement.executeQuery();
            if (resultSet.next()){
                return Game.builder()
                        .ID(resultSet.getLong("game_id"))
                        .date(resultSet.getString("begins"))
                        .firstPlayerId(resultSet.getLong("player1_id"))
                        .firstPlayerShotsCount(resultSet.getInt("player1_shots"))
                        .secondPlayerId(resultSet.getLong("player2_id"))
//                        .secondPlayer()
                        .secondPlayerShotsCount(resultSet.getInt("player2_shots"))
                        .duration(resultSet.getString("game_duration")).build();


            }
            return null;
        } catch (SQLException e ){
            throw new IllegalStateException(e);
        }
    }

    @Override
    public void addPlayersToGame(Long gameId, Player firstPlayer, Player secondPlayer) {
        try (Connection connection = dataSource.getConnection();
        PreparedStatement preparedStatement = connection.prepareStatement(SQL_INSERT_PLAYERS_IN_GAME)){
            preparedStatement.setLong(1, firstPlayer.getPlayerID());
            preparedStatement.setLong(2, secondPlayer.getPlayerID());
            preparedStatement.setLong(3, gameId);

            int insertRowsCount = preparedStatement.executeUpdate();
            if (insertRowsCount == 0){
                System.out.println("Cannot insert players in game");
            }

        }
        catch (SQLException e){
            throw new IllegalStateException( e);
        }
    }

    @Override
    public void update(Game game) {

//        //language=sql
//        public static final String SQL_UPDATE = "update games " +
//                "set begins = ?, player1_id = ?, player1_shots = ?," +
//                "player2_id = ?, player2_shots = ? where game_id = ?" +
//                ";";
        try(Connection connection = dataSource.getConnection();
        PreparedStatement preparedStatement = connection.prepareStatement(  SQL_UPDATE)){
            preparedStatement.setString(1, game.getDate());
            preparedStatement.setLong(2, game.getFirstPlayerId());
            preparedStatement.setInt(3, game.getFirstPlayerShotsCount());
            preparedStatement.setLong(4, game.getSecondPlayerId());
            preparedStatement.setInt(5, game.getSecondPlayerShotsCount());
            preparedStatement.setString(6, game.getDuration());
            preparedStatement.setLong(7, game.getID());

            int updateRowsCount = preparedStatement.executeUpdate();
            if (updateRowsCount == 0){
                System.out.println("Cannot update game");
            }
        }
        catch (SQLException e){
            throw new IllegalStateException(e);
        }
    }
}
