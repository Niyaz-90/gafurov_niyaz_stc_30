package inno.models;

import lombok.*;

import java.time.LocalTime;

@Data
@AllArgsConstructor
@Builder


public class Shot {
    private Long shotId;
    private Game game;
    private Player shooter;
    private Player target;
    // h:m:s
    private String shotTime;
    private boolean hitTheTarget;

    public Shot(Game game, Player shooter, Player target) {
        this.game = game;
        this.shooter = shooter;
        this.target = target;
    }

    public String getShotTime() {
        return LocalTime.now().toString();
    }
}
